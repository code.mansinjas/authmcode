import { UserInterface } from '@/app/auth/page'
import { LoginApi, SignUpApi } from '@/constants/apiUrl'
import { GlobalResponse } from '@/constants/common.type'
import axios, { AxiosError } from 'axios'

export const LoginHook = async ({ email, password }: { email: string, password: string }): Promise<GlobalResponse & { status: number }> => {
    try {
        const result = await axios.post<GlobalResponse>(LoginApi, { email, password })
        return { status: result.status, ...result.data }
    } catch (err: any) {
        return { status: 500, success: false, error: err, message: err.message }
    }

}

export const SignInHook = async ({ email, password, username }: UserInterface): Promise<GlobalResponse & { status: number }> => {
    try {
        const result = await axios.post<GlobalResponse>(SignUpApi, { email, password, username })
        return { status: result.status, ...result.data }
    } catch (err: any) {
        return { status: 400, success: false, error: err, message: err?.response?.data?.message }
    }

}