import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Please provide Username"],
        unique: true
    },
    email: {
        type: String,
        required: [true, "Please provide Email"],
        unique: true
    },
    password: {
        type: String,
        required: [true, "Please provide Password"],
        unique: true
    },
    isVerified: {
        type: Boolean,
        default: false
    },
    verifyToken: {
        type: String
    },
    verifyResetPassToken: {
        type: String
    },
    verifyTimeExpiry:{
        type: Number
    }
})

const User = mongoose.models.users || mongoose.model("users", UserSchema)

export default User