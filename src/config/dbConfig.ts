import mongoose from 'mongoose'

export async function connect() {
    try {
        mongoose.connect(process.env.MONGO_URI!)
        const connection = mongoose.connection
        connection.on('connected', () => {
            console.log("MongoDB Connected")
        })
        connection.on('error', (err) => {
            console.log("Error while Connecting MongoDB \n", err)
            process.exit()
        })
    } catch (ex) {
        console.log("Error while Connecting MongoDB \n", ex)
    }
}