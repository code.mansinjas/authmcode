"use client"
import { LoginHook, SignInHook } from '@/hooks/common'
import { useRouter, useSearchParams } from 'next/navigation'
import React, { useState } from 'react'
import toast, { Toaster } from 'react-hot-toast'

export interface UserInterface {
  username?: string,
  email: string,
  password: string
}

const LoginSignup = () => {
  const page = useSearchParams().get('page')
  const router = useRouter()

  const [loginPage, SetLoginPage] = useState<boolean>(page == 'login')
  const [user, SetUser] = useState<UserInterface>({ username: '', email: '', password: '' })

  const OnSwitchBtnClick = (page: 'login' | 'signup') => {
    SetLoginPage(!loginPage)
    SetUser({ email: '', password: '', username: '' })
    router.push(`/auth?page=${page}`)
  }

  const OnSubmit = async (page: 'login' | 'signup') => {
    const QueryFun = page == 'login' ? LoginHook({ email: user.email, password: user.password }) : SignInHook(user)
    const mainResponse = await QueryFun
    const { success, message, error, status } = mainResponse
    if(success){
      toast.success(message || 'Successfull')
      router.push(`/dashboard`)
    }else{
      toast.error(message || error?.toString() || `Error while ${page} Data` )
    }
  }

  const CheckValidate = () => {

  }
  return (
    <>
      <div className="main-container">
        <div className="sub-container">
          <div className='btn-container'>
            <button className="btn switch-btn" disabled={loginPage} onClick={() => OnSwitchBtnClick('login')}>Login</button>
            <button className="btn switch-btn" disabled={!loginPage} onClick={() => OnSwitchBtnClick('signup')}>Sign Up</button>
          </div>
          <label className="input input-bordered flex items-center gap-2">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" className="w-4 h-4 opacity-70"><path d="M2.5 3A1.5 1.5 0 0 0 1 4.5v.793c.026.009.051.02.076.032L7.674 8.51c.206.1.446.1.652 0l6.598-3.185A.755.755 0 0 1 15 5.293V4.5A1.5 1.5 0 0 0 13.5 3h-11Z" /><path d="M15 6.954 8.978 9.86a2.25 2.25 0 0 1-1.956 0L1 6.954V11.5A1.5 1.5 0 0 0 2.5 13h11a1.5 1.5 0 0 0 1.5-1.5V6.954Z" /></svg>
            <input type="text" className="grow" placeholder="Email" value={user.email} onChange={e => SetUser({ ...user, email: e.target.value })} />
          </label>
          {loginPage ? <></> :
            <label className="input input-bordered flex items-center gap-2">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" className="w-4 h-4 opacity-70"><path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6ZM12.735 14c.618 0 1.093-.561.872-1.139a6.002 6.002 0 0 0-11.215 0c-.22.578.254 1.139.872 1.139h9.47Z" /></svg>
              <input type="text" className="grow" placeholder="Username" value={user.username} onChange={e => SetUser({ ...user, username: e.target.value })} />
            </label>}
          <label className="input input-bordered flex items-center gap-2">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16" fill="currentColor" className="w-4 h-4 opacity-70"><path fillRule="evenodd" d="M14 6a4 4 0 0 1-4.899 3.899l-1.955 1.955a.5.5 0 0 1-.353.146H5v1.5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1-.5-.5v-2.293a.5.5 0 0 1 .146-.353l3.955-3.955A4 4 0 1 1 14 6Zm-4-2a.75.75 0 0 0 0 1.5.5.5 0 0 1 .5.5.75.75 0 0 0 1.5 0 2 2 0 0 0-2-2Z" clipRule="evenodd" /></svg>
            <input type="password" className="grow" placeholder="password" value={user.password} onChange={e => SetUser({ ...user, password: e.target.value })} />
          </label>
          <button className="btn" onClick={() => OnSubmit(loginPage ? 'login' : 'signup')} >{loginPage ? 'Login' : 'SignUp'}</button>
        </div>
        <Toaster position='top-right' />
      </div>
      <style jsx>
        {`
        .main-container{
          height: 100vh;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        .sub-container{
          height: 50vh;
          display: flex;
          flex-direction: column;
          row-gap: 20px;
        }

        .btn-container .switch-btn{
          width: 50%;
        }
      `}
      </style>
    </>
  )
}

export default LoginSignup
