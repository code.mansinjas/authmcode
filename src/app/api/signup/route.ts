import { NextRequest, NextResponse } from "next/server";
import { connect } from "@/config/dbConfig"
import bcryptjs from "bcryptjs"
import User from "@/models/user";
import { GlobalResponse } from "@/constants/common.type";
import { GenerateToken } from "../common/token";

connect()

interface SignInDto {
    username: string
    email: string
    password: string
}



export async function POST(request: NextRequest) {
    const { email = '', password = '', username = '' }: SignInDto = await request.json()
    if (!(email && password && username)) {
        return NextResponse.json<GlobalResponse>({
            success: false,
            message: "Please Enter Username, email and Password"
        }, { status: 404 })
    }

    const userExist = await User.findOne({ email })

    if (userExist) {
        return NextResponse.json<GlobalResponse>({
            success: false,
            message: "User Already Exist"
        }, { status: 400 })
    }

    const salt = await bcryptjs.genSalt(10)
    const hashedPass = await bcryptjs.hash(password, salt)

    const result = new User({
        email, username, password: hashedPass
    })

    const user = await result.save()
    const token = GenerateToken({ email: user?.email, username: user?.username, _id: user?._id })
    if (!token) {
        return NextResponse.json<GlobalResponse>({
            success: false,
            message: "Error while Generating Token"
        }, { status: 404 })
    }
    const response = NextResponse.json<GlobalResponse>({
        success: true,
        message: "User Created Successfully"
    }, { status: 201 })
    response.cookies.set('token', token, {
        httpOnly: true
    })
    return response

}