import jwt from 'jsonwebtoken'
const SECRET = 'qwertryuiop'

export const GenerateToken = (user: { _id: string, email: string, username: string }) => {
    try {
        const { email, username, _id } = user
        const token = jwt.sign({ email, username, _id }, SECRET, {
            expiresIn: '1d'
        })
        return token
    } catch (err) {
        return false
    }
}

export const VerifyToken = (payload: string) => {
    try {
        const decoded = jwt.verify(payload, SECRET)
        return decoded
    } catch (err) {
        return false
    }
}