import { GlobalResponse } from "@/constants/common.type";
import User from "@/models/user";
import { NextRequest, NextResponse } from "next/server";
import { GenerateToken } from "../common/token";
import bcryptjs from "bcryptjs"

export async function POST(request: NextRequest) {
    const { email = '', password = '' } = await request.json()
    const userExist = await User.findOne({ email })
    if (!userExist) {
        return NextResponse.json<GlobalResponse>({
            success: false,
            message: "User Not Exist"
        }, { status: 404 })
    }
    if (!(bcryptjs.compareSync(password, userExist?.password))) {
        return NextResponse.json<GlobalResponse>({
            success: false,
            message: "Invalid Password"
        }, { status: 404 })
    }
    const token = GenerateToken({ email: userExist?.email, username: userExist?.username, _id: userExist?._id })
    if (!token) {
        return NextResponse.json<GlobalResponse>({
            success: false,
            message: "Error while Generating Token"
        }, { status: 404 })
    }
    const response = NextResponse.json({
        success: true,
        message: "User Logged In Successfully"
    })
    response.cookies.set('token', token, {
        httpOnly: true
    })
    return response
}