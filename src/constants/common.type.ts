export interface GlobalResponse {
    success: boolean
    error?: Error | string
    message?: string
    data?: any
}