let BACKEND_URL = process.env.BACKEND_URL || ''

BACKEND_URL = `${BACKEND_URL}/api`

export const LoginApi = `${BACKEND_URL}/login`

export const SignUpApi = `${BACKEND_URL}/signup`