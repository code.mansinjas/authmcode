import { redirect } from 'next/dist/server/api-utils'
import { NextResponse } from 'next/server'
import type { NextRequest } from 'next/server'

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  const token = request.cookies.get('token')?.value
  if (!token && !(request.nextUrl.pathname == '/auth')) {
    return NextResponse.redirect(new URL('/auth?page=login', request.nextUrl))
  }
  if (token && request.nextUrl.pathname == '/auth') {
    return NextResponse.redirect(new URL('/dashboard', request.nextUrl))
  }
  console.log('Middleware called for', request.nextUrl.pathname, request.nextUrl.searchParams)
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: [
    '/((?!api|_next/static|_next/image|favicon.ico).*)',
  ],
}